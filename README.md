### Usage
```sh
cat <<'EOF' | sudo tee /etc/apt/sources.list.d/customrepo.list >/dev/null
deb [ trusted=yes ] file:///path/to/this/folder/ buster main non-free contrib
EOF
sudo apt-get update
```
