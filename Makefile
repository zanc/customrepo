DISTRIBUTION := buster

CHANGES_PATH :=
CHANGES_PATH += ../source/ungoogled-chromium
CHANGES_PATH += ../source/dwm
CHANGES_PATH += ../source/suckless-tools
#CHANGES_PATH += ../source/megatools
#CHANGES_PATH += ../source/nzbget
CHANGES_PATH += ../source/stterm
CHANGES_PATH += ../source/xfonts-siji
CHANGES_PATH += ../source/zathura-pdf-mupdf
CHANGES_PATH += ../source/b43-fwcutter
CHANGES_PATH += ../source/fonts-ionicons
#CHANGES_PATH += ../source/virtualbox-ext-pack
CHANGES_PATH += ../source/megacmd
CHANGES_PATH += ../source/fantome-gtk-theme
CHANGES_PATH += ../source/arc-icon-theme
CHANGES_PATH += ../source/xv
CHANGES_PATH += ../source/chiaki
CHANGES_PATH += ../source/sxiv
CHANGES_PATH += ../source/qutebrowser
CHANGES_PATH += ../source/i3lock-color
CHANGES_PATH += ../source/i3lock-fancy
CHANGES_PATH += ../source/xft
CHANGES_PATH += ../source/gcc-doc
CHANGES_PATH += ../source/gcc-doc-defaults
#CHANGES_PATH += ../source/golang-github-soniakeys-bits
#CHANGES_PATH += ../source/graph
#CHANGES_PATH += ../source/golang-github-sabhiram-go-git-ignore
#CHANGES_PATH += ../source/gdrive

upload:
	@for p in ${CHANGES_PATH}; do \
		for c in $$p/*.changes; do \
			if ! grep -q -e '^Distribution:\s*${DISTRIBUTION}' $$c; then \
				sed -i -e '/^Distribution:\s*.*/s//Distribution: ${DISTRIBUTION}/' $$c; \
			fi; \
			reprepro include ${DISTRIBUTION} $$c; \
		done; \
	done
	@find pool -type f \( -iname '*.deb' -o -iname '*.udeb' \) | while read link; do \
		deb="$${link##*/}"; \
		for p in ${CHANGES_PATH}; do \
			if [ -e "$$p/$$deb" ]; then \
				ln -srf "$$p/$$deb" "$$link"; \
				break; \
			fi; \
		done; \
	done

remove:
	@packages=$$(find ${CHANGES_PATH} -type f \( -iname '*.deb' -o -iname '*.udeb' \) | sed -e 's,.*/,,' -e 's,_.*,,'); \
	reprepro remove ${DISTRIBUTION} $$packages

serve:
	@python3 -m http.server 9997

.PHONY: upload remove serve
